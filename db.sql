-- MySQL dump 10.13  Distrib 5.7.20, for Linux (x86_64)
--
-- Host: localhost    Database: leaverequester
-- ------------------------------------------------------
-- Server version	5.7.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `databasechangelog`
--
drop database IF EXISTS leaverequester;
create database leaverequester;
use leaverequester;
DROP TABLE IF EXISTS `databasechangelog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `databasechangelog` (
  `ID` varchar(255) NOT NULL,
  `AUTHOR` varchar(255) NOT NULL,
  `FILENAME` varchar(255) NOT NULL,
  `DATEEXECUTED` datetime NOT NULL,
  `ORDEREXECUTED` int(11) NOT NULL,
  `EXECTYPE` varchar(10) NOT NULL,
  `MD5SUM` varchar(35) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `COMMENTS` varchar(255) DEFAULT NULL,
  `TAG` varchar(255) DEFAULT NULL,
  `LIQUIBASE` varchar(20) DEFAULT NULL,
  `CONTEXTS` varchar(255) DEFAULT NULL,
  `LABELS` varchar(255) DEFAULT NULL,
  `DEPLOYMENT_ID` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `databasechangelog`
--

LOCK TABLES `databasechangelog` WRITE;
/*!40000 ALTER TABLE `databasechangelog` DISABLE KEYS */;
INSERT INTO `databasechangelog` VALUES ('00000000000001','jhipster','config/liquibase/changelog/00000000000000_initial_schema.xml','2019-04-25 17:18:19',1,'EXECUTED','7:c42360db095418f2763969902443e294','createTable tableName=jhi_user; createTable tableName=jhi_authority; createTable tableName=jhi_user_authority; addPrimaryKey tableName=jhi_user_authority; createTable tableName=jhi_persistent_token; addForeignKeyConstraint baseTableName=jhi_user_a...','',NULL,'3.5.4',NULL,NULL,'6212698409'),('20190425171347-1','jhipster','config/liquibase/changelog/20190425171347_added_entity_LeaveRequeste.xml','2019-04-25 17:18:19',2,'EXECUTED','7:599cc63a36ba4d532e89c434ec370a84','createTable tableName=leave_requeste','',NULL,'3.5.4',NULL,NULL,'6212698409'),('20190425171347-2','jhipster','config/liquibase/changelog/20190425171347_added_entity_constraints_LeaveRequeste.xml','2019-04-25 17:18:19',3,'EXECUTED','7:6c74206ecd65ec601f845cbabbd5142d','addForeignKeyConstraint baseTableName=leave_requeste, constraintName=fk_leave_requeste_employee_id, referencedTableName=jhi_user','',NULL,'3.5.4',NULL,NULL,'6212698409');
/*!40000 ALTER TABLE `databasechangelog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `databasechangeloglock`
--

DROP TABLE IF EXISTS `databasechangeloglock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `databasechangeloglock` (
  `ID` int(11) NOT NULL,
  `LOCKED` bit(1) NOT NULL,
  `LOCKGRANTED` datetime DEFAULT NULL,
  `LOCKEDBY` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `databasechangeloglock`
--

LOCK TABLES `databasechangeloglock` WRITE;
/*!40000 ALTER TABLE `databasechangeloglock` DISABLE KEYS */;
INSERT INTO `databasechangeloglock` VALUES (1,'\0',NULL,NULL);
/*!40000 ALTER TABLE `databasechangeloglock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jhi_authority`
--

DROP TABLE IF EXISTS `jhi_authority`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jhi_authority` (
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jhi_authority`
--

LOCK TABLES `jhi_authority` WRITE;
/*!40000 ALTER TABLE `jhi_authority` DISABLE KEYS */;
INSERT INTO `jhi_authority` VALUES ('ROLE_ADMIN'),('ROLE_USER');
/*!40000 ALTER TABLE `jhi_authority` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jhi_persistent_audit_event`
--

DROP TABLE IF EXISTS `jhi_persistent_audit_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jhi_persistent_audit_event` (
  `event_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `principal` varchar(50) NOT NULL,
  `event_date` timestamp NULL DEFAULT NULL,
  `event_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`event_id`),
  KEY `idx_persistent_audit_event` (`principal`,`event_date`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jhi_persistent_audit_event`
--

LOCK TABLES `jhi_persistent_audit_event` WRITE;
/*!40000 ALTER TABLE `jhi_persistent_audit_event` DISABLE KEYS */;
INSERT INTO `jhi_persistent_audit_event` VALUES (1,'admin','2019-04-25 17:18:58','AUTHENTICATION_SUCCESS'),(2,'boly','2019-04-25 18:01:45','AUTHENTICATION_SUCCESS'),(3,'boly','2019-04-25 21:41:22','AUTHENTICATION_SUCCESS'),(4,'seynou','2019-04-25 21:43:53','AUTHENTICATION_SUCCESS'),(5,'boly','2019-04-25 21:44:48','AUTHENTICATION_SUCCESS'),(6,'boly','2019-04-25 21:55:09','AUTHENTICATION_SUCCESS'),(7,'boly','2019-04-26 00:10:51','AUTHENTICATION_FAILURE'),(8,'boly','2019-04-26 00:10:55','AUTHENTICATION_SUCCESS'),(9,'boly','2019-04-26 00:40:43','AUTHENTICATION_SUCCESS'),(10,'seynou','2019-04-26 00:47:51','AUTHENTICATION_SUCCESS'),(11,'admin','2019-04-26 00:56:56','AUTHENTICATION_SUCCESS'),(12,'boly','2019-04-26 08:20:34','AUTHENTICATION_SUCCESS'),(13,'admin','2019-04-26 08:43:19','AUTHENTICATION_SUCCESS'),(14,'undefined','2019-04-26 08:43:19','AUTHENTICATION_FAILURE'),(15,'admin','2019-04-26 08:43:36','AUTHENTICATION_SUCCESS'),(16,'boly','2019-04-26 08:56:07','AUTHENTICATION_SUCCESS'),(17,'boly','2019-04-26 09:05:34','AUTHENTICATION_SUCCESS'),(18,'boly','2019-04-26 09:08:56','AUTHENTICATION_SUCCESS'),(19,'boly','2019-04-26 10:16:06','AUTHENTICATION_SUCCESS'),(20,'boly','2019-04-26 10:24:00','AUTHENTICATION_SUCCESS'),(21,'boly','2019-04-26 10:26:15','AUTHENTICATION_SUCCESS'),(22,'boly','2019-04-26 10:27:29','AUTHENTICATION_SUCCESS'),(23,'boly','2019-04-26 10:32:58','AUTHENTICATION_SUCCESS'),(24,'boly','2019-04-26 10:33:13','AUTHENTICATION_SUCCESS'),(25,'boly','2019-04-26 10:34:29','AUTHENTICATION_SUCCESS'),(26,'boly','2019-04-26 10:37:09','AUTHENTICATION_SUCCESS'),(27,'boly','2019-04-26 10:37:26','AUTHENTICATION_SUCCESS'),(28,'boly','2019-04-26 10:39:03','AUTHENTICATION_SUCCESS'),(29,'boly','2019-04-26 10:39:37','AUTHENTICATION_SUCCESS'),(30,'boly','2019-04-26 10:42:44','AUTHENTICATION_SUCCESS'),(31,'boly','2019-04-26 10:44:42','AUTHENTICATION_SUCCESS'),(32,'boly','2019-04-26 10:47:06','AUTHENTICATION_SUCCESS'),(33,'admin','2019-04-26 10:48:37','AUTHENTICATION_SUCCESS'),(34,'moddy','2019-04-26 10:53:02','AUTHENTICATION_FAILURE'),(35,'boly','2019-04-26 10:53:08','AUTHENTICATION_SUCCESS'),(36,'boly','2019-04-26 10:53:52','AUTHENTICATION_SUCCESS'),(37,'admin','2019-04-26 10:54:24','AUTHENTICATION_FAILURE'),(38,'admin','2019-04-26 10:54:28','AUTHENTICATION_SUCCESS'),(39,'boly','2019-04-26 10:56:13','AUTHENTICATION_SUCCESS'),(40,'admin','2019-04-26 10:56:24','AUTHENTICATION_FAILURE'),(41,'admin','2019-04-26 10:56:27','AUTHENTICATION_SUCCESS'),(42,'boly','2019-04-26 10:57:33','AUTHENTICATION_SUCCESS'),(43,'boly','2019-04-26 10:58:47','AUTHENTICATION_SUCCESS'),(44,'admin','2019-04-26 11:00:58','AUTHENTICATION_SUCCESS'),(45,'boly','2019-04-26 11:02:09','AUTHENTICATION_SUCCESS'),(46,'boly','2019-04-26 11:07:44','AUTHENTICATION_FAILURE'),(47,'boly','2019-04-26 11:07:49','AUTHENTICATION_SUCCESS'),(48,'admin','2019-04-26 11:09:15','AUTHENTICATION_FAILURE'),(49,'admin','2019-04-26 11:09:18','AUTHENTICATION_SUCCESS'),(50,'boly','2019-04-26 11:10:21','AUTHENTICATION_SUCCESS'),(51,'boly','2019-04-26 11:11:38','AUTHENTICATION_SUCCESS'),(52,'boly','2019-04-26 11:12:30','AUTHENTICATION_SUCCESS'),(53,'boly','2019-04-26 11:14:31','AUTHENTICATION_SUCCESS'),(54,'boly','2019-04-26 11:14:38','AUTHENTICATION_SUCCESS'),(55,'boly','2019-04-26 11:24:01','AUTHENTICATION_SUCCESS'),(56,'boly','2019-04-26 11:24:11','AUTHENTICATION_SUCCESS'),(57,'boly','2019-04-26 11:24:52','AUTHENTICATION_SUCCESS'),(58,'admin','2019-04-26 11:25:14','AUTHENTICATION_FAILURE'),(59,'admin','2019-04-26 11:25:18','AUTHENTICATION_SUCCESS'),(60,'boly','2019-04-26 11:25:40','AUTHENTICATION_SUCCESS'),(61,'boly','2019-04-26 11:26:38','AUTHENTICATION_SUCCESS'),(62,'boly','2019-04-26 11:26:51','AUTHENTICATION_SUCCESS'),(63,'boly','2019-04-26 11:27:03','AUTHENTICATION_SUCCESS'),(64,'admin','2019-04-26 11:27:21','AUTHENTICATION_SUCCESS'),(65,'boly','2019-04-26 11:27:56','AUTHENTICATION_SUCCESS'),(66,'boly','2019-04-26 11:28:47','AUTHENTICATION_SUCCESS'),(67,'boly','2019-04-26 11:29:56','AUTHENTICATION_SUCCESS'),(68,'boly','2019-04-26 11:30:46','AUTHENTICATION_SUCCESS'),(69,'boly','2019-04-26 11:32:14','AUTHENTICATION_SUCCESS'),(70,'boly','2019-04-26 11:32:50','AUTHENTICATION_SUCCESS'),(71,'boly','2019-04-26 11:33:04','AUTHENTICATION_SUCCESS'),(72,'admin','2019-04-26 11:33:19','AUTHENTICATION_SUCCESS'),(73,'boly','2019-04-26 11:33:34','AUTHENTICATION_SUCCESS'),(74,'boly','2019-04-26 11:34:40','AUTHENTICATION_SUCCESS'),(75,'admin','2019-04-26 11:34:52','AUTHENTICATION_SUCCESS'),(76,'boly','2019-04-26 11:35:10','AUTHENTICATION_SUCCESS'),(77,'admin','2019-04-26 11:36:27','AUTHENTICATION_SUCCESS'),(78,'boly','2019-04-26 11:38:46','AUTHENTICATION_SUCCESS'),(79,'admin','2019-04-26 11:39:09','AUTHENTICATION_SUCCESS'),(80,'boly','2019-04-26 11:39:24','AUTHENTICATION_SUCCESS'),(81,'boly','2019-04-26 11:43:01','AUTHENTICATION_SUCCESS'),(82,'boly','2019-04-26 11:43:26','AUTHENTICATION_SUCCESS'),(83,'undefined','2019-04-26 11:43:26','AUTHENTICATION_FAILURE'),(84,'boly','2019-04-26 11:45:36','AUTHENTICATION_SUCCESS'),(85,'undefined','2019-04-26 11:45:36','AUTHENTICATION_FAILURE'),(86,'undefined','2019-04-26 11:45:37','AUTHENTICATION_FAILURE'),(87,'boly','2019-04-26 11:46:23','AUTHENTICATION_SUCCESS'),(88,'undefined','2019-04-26 11:46:23','AUTHENTICATION_FAILURE'),(89,'undefined','2019-04-26 11:46:24','AUTHENTICATION_FAILURE');
/*!40000 ALTER TABLE `jhi_persistent_audit_event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jhi_persistent_audit_evt_data`
--

DROP TABLE IF EXISTS `jhi_persistent_audit_evt_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jhi_persistent_audit_evt_data` (
  `event_id` bigint(20) NOT NULL,
  `name` varchar(150) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`event_id`,`name`),
  KEY `idx_persistent_audit_evt_data` (`event_id`),
  CONSTRAINT `fk_evt_pers_audit_evt_data` FOREIGN KEY (`event_id`) REFERENCES `jhi_persistent_audit_event` (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jhi_persistent_audit_evt_data`
--

LOCK TABLES `jhi_persistent_audit_evt_data` WRITE;
/*!40000 ALTER TABLE `jhi_persistent_audit_evt_data` DISABLE KEYS */;
INSERT INTO `jhi_persistent_audit_evt_data` VALUES (1,'remoteAddress','127.0.0.1'),(2,'remoteAddress','127.0.0.1'),(3,'remoteAddress','127.0.0.1'),(4,'remoteAddress','127.0.0.1'),(5,'remoteAddress','127.0.0.1'),(6,'remoteAddress','127.0.0.1'),(7,'message','Bad credentials'),(7,'remoteAddress','127.0.0.1'),(7,'type','org.springframework.security.authentication.BadCredentialsException'),(8,'remoteAddress','127.0.0.1'),(9,'remoteAddress','127.0.0.1'),(10,'remoteAddress','0:0:0:0:0:0:0:1'),(11,'remoteAddress','0:0:0:0:0:0:0:1'),(12,'remoteAddress','0:0:0:0:0:0:0:1'),(13,'remoteAddress','127.0.0.1'),(14,'message','Bad credentials'),(14,'remoteAddress','127.0.0.1'),(14,'type','org.springframework.security.authentication.BadCredentialsException'),(15,'remoteAddress','127.0.0.1'),(16,'remoteAddress','127.0.0.1'),(17,'remoteAddress','127.0.0.1'),(18,'remoteAddress','127.0.0.1'),(19,'remoteAddress','127.0.0.1'),(20,'remoteAddress','127.0.0.1'),(20,'sessionId','okzCEU0O6bSjjsR-MVNbonV5w6kFDig_Gw1empQt'),(21,'remoteAddress','127.0.0.1'),(21,'sessionId','G904y8GNzKRniBFqjNvROm_2qShitxHkwgRz_snR'),(22,'remoteAddress','127.0.0.1'),(22,'sessionId','ymG8Bb2ys7tGg143FS-TOtnxnpfZc0bdvUMPXDvn'),(23,'remoteAddress','127.0.0.1'),(23,'sessionId','YfECLx3H9cLAbIDxpQNtAtR2UDW9a0hkhB10ln7b'),(24,'remoteAddress','127.0.0.1'),(24,'sessionId','H3vjxkEd8AcFn8H6CUm-730jchDSX6kB5XNcET5e'),(25,'remoteAddress','127.0.0.1'),(25,'sessionId','FYeXZPq6kAQEq38HyE2qNwPQFAeG6SF4FZh2sDx3'),(26,'remoteAddress','127.0.0.1'),(27,'remoteAddress','127.0.0.1'),(27,'sessionId','Z6rLlnko2M6W4kINJQRk_jEEdDfeUVhXoySulrFC'),(28,'remoteAddress','127.0.0.1'),(29,'remoteAddress','127.0.0.1'),(30,'remoteAddress','127.0.0.1'),(31,'remoteAddress','127.0.0.1'),(32,'remoteAddress','127.0.0.1'),(33,'remoteAddress','127.0.0.1'),(34,'message','Bad credentials'),(34,'remoteAddress','127.0.0.1'),(34,'type','org.springframework.security.authentication.BadCredentialsException'),(35,'remoteAddress','127.0.0.1'),(36,'remoteAddress','127.0.0.1'),(37,'message','Bad credentials'),(37,'remoteAddress','127.0.0.1'),(37,'type','org.springframework.security.authentication.BadCredentialsException'),(38,'remoteAddress','127.0.0.1'),(39,'remoteAddress','127.0.0.1'),(40,'message','Bad credentials'),(40,'remoteAddress','127.0.0.1'),(40,'type','org.springframework.security.authentication.BadCredentialsException'),(41,'remoteAddress','127.0.0.1'),(42,'remoteAddress','127.0.0.1'),(43,'remoteAddress','127.0.0.1'),(44,'remoteAddress','127.0.0.1'),(45,'remoteAddress','127.0.0.1'),(46,'message','Bad credentials'),(46,'remoteAddress','127.0.0.1'),(46,'type','org.springframework.security.authentication.BadCredentialsException'),(47,'remoteAddress','127.0.0.1'),(48,'message','Bad credentials'),(48,'remoteAddress','127.0.0.1'),(48,'type','org.springframework.security.authentication.BadCredentialsException'),(49,'remoteAddress','127.0.0.1'),(50,'remoteAddress','127.0.0.1'),(51,'remoteAddress','127.0.0.1'),(52,'remoteAddress','127.0.0.1'),(53,'remoteAddress','127.0.0.1'),(54,'remoteAddress','127.0.0.1'),(55,'remoteAddress','127.0.0.1'),(56,'remoteAddress','127.0.0.1'),(57,'remoteAddress','127.0.0.1'),(58,'message','Bad credentials'),(58,'remoteAddress','127.0.0.1'),(58,'type','org.springframework.security.authentication.BadCredentialsException'),(59,'remoteAddress','127.0.0.1'),(60,'remoteAddress','127.0.0.1'),(61,'remoteAddress','127.0.0.1'),(62,'remoteAddress','127.0.0.1'),(63,'remoteAddress','127.0.0.1'),(64,'remoteAddress','127.0.0.1'),(65,'remoteAddress','127.0.0.1'),(66,'remoteAddress','127.0.0.1'),(67,'remoteAddress','127.0.0.1'),(68,'remoteAddress','127.0.0.1'),(69,'remoteAddress','127.0.0.1'),(70,'remoteAddress','127.0.0.1'),(71,'remoteAddress','127.0.0.1'),(72,'remoteAddress','127.0.0.1'),(73,'remoteAddress','127.0.0.1'),(74,'remoteAddress','127.0.0.1'),(75,'remoteAddress','127.0.0.1'),(76,'remoteAddress','127.0.0.1'),(77,'remoteAddress','127.0.0.1'),(78,'remoteAddress','127.0.0.1'),(79,'remoteAddress','127.0.0.1'),(80,'remoteAddress','127.0.0.1'),(81,'remoteAddress','127.0.0.1'),(82,'remoteAddress','127.0.0.1'),(83,'message','Bad credentials'),(83,'remoteAddress','127.0.0.1'),(83,'type','org.springframework.security.authentication.BadCredentialsException'),(84,'remoteAddress','127.0.0.1'),(85,'message','Bad credentials'),(85,'remoteAddress','127.0.0.1'),(85,'type','org.springframework.security.authentication.BadCredentialsException'),(86,'message','Bad credentials'),(86,'remoteAddress','127.0.0.1'),(86,'sessionId','_mrhdqCXOPhdthfkTbl9ThPsY8f_57sYnSAqcZFE'),(86,'type','org.springframework.security.authentication.BadCredentialsException'),(87,'remoteAddress','127.0.0.1'),(88,'message','Bad credentials'),(88,'remoteAddress','127.0.0.1'),(88,'type','org.springframework.security.authentication.BadCredentialsException'),(89,'message','Bad credentials'),(89,'remoteAddress','127.0.0.1'),(89,'sessionId','o5Awi9FwrjXO_ZPdW-mCNTJiIaaPF0ZEaXhYkgxd'),(89,'type','org.springframework.security.authentication.BadCredentialsException');
/*!40000 ALTER TABLE `jhi_persistent_audit_evt_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jhi_persistent_token`
--

DROP TABLE IF EXISTS `jhi_persistent_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jhi_persistent_token` (
  `series` varchar(20) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `token_value` varchar(20) NOT NULL,
  `token_date` date DEFAULT NULL,
  `ip_address` varchar(39) DEFAULT NULL,
  `user_agent` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`series`),
  KEY `fk_user_persistent_token` (`user_id`),
  CONSTRAINT `fk_user_persistent_token` FOREIGN KEY (`user_id`) REFERENCES `jhi_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jhi_persistent_token`
--

LOCK TABLES `jhi_persistent_token` WRITE;
/*!40000 ALTER TABLE `jhi_persistent_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `jhi_persistent_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jhi_user`
--

DROP TABLE IF EXISTS `jhi_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jhi_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `login` varchar(50) NOT NULL,
  `password_hash` varchar(60) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `email` varchar(191) DEFAULT NULL,
  `image_url` varchar(256) DEFAULT NULL,
  `activated` bit(1) NOT NULL,
  `lang_key` varchar(6) DEFAULT NULL,
  `activation_key` varchar(20) DEFAULT NULL,
  `reset_key` varchar(20) DEFAULT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_date` timestamp NULL,
  `reset_date` timestamp NULL DEFAULT NULL,
  `last_modified_by` varchar(50) DEFAULT NULL,
  `last_modified_date` timestamp NULL DEFAULT NULL,
  `superior_login` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_user_login` (`login`),
  UNIQUE KEY `ux_user_email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jhi_user`
--

LOCK TABLES `jhi_user` WRITE;
/*!40000 ALTER TABLE `jhi_user` DISABLE KEYS */;
INSERT INTO `jhi_user` VALUES (1,'system','$2a$10$mE.qmcV0mFU5NcKh73TZx.z4ueI/.bDWbj0T1BYyqP481kGGarKLG','System','System','system@localhost','','','en',NULL,NULL,'system',NULL,NULL,'system',NULL,NULL),(2,'anonymoususer','$2a$10$j8S5d7Sr7.8VTOYNviDPOeWX8KcYILUVJBsYV83Y5NtECayypx9lO','Anonymous','User','anonymous@localhost','','','en',NULL,NULL,'system',NULL,NULL,'system',NULL,NULL),(3,'admin','$2a$10$gSAhZrxMllrbgj/kkK9UceBPpChGWJA7SYIb1Mqo.n5aNLq1/oRrC','Administrator','Administrator','admin@localhost','','','en',NULL,NULL,'system',NULL,NULL,'system',NULL,NULL),(4,'user','$2a$10$VEjxo0jq2YG9Rbk2HmX9S.k1uZBGYUHdUcid3g/vfiEl7lwWgOH/K','User','User','user@localhost','','','en',NULL,NULL,'system',NULL,NULL,'system',NULL,NULL),(19,'boly','$2a$10$u3nVYltJiviKbx0upFJp1upJmctMC0jaGqMXxnde4yEWipSgAUXGG',NULL,NULL,'modoulobolysow@gmail.com',NULL,'','en',NULL,NULL,'anonymousUser','2019-04-25 21:40:56',NULL,'anonymousUser','2019-04-25 21:41:14','admin'),(20,'seynou','$2a$10$AKf9NWoY5T0Xnd8IFppJnO.htejm6CFWl4JCRkmW9GHy0hSc.a//.',NULL,NULL,'jeumodoulobolysow@gmail.com',NULL,'','en',NULL,NULL,'anonymousUser','2019-04-25 21:43:24',NULL,'anonymousUser','2019-04-25 21:43:39','boly');
/*!40000 ALTER TABLE `jhi_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jhi_user_authority`
--

DROP TABLE IF EXISTS `jhi_user_authority`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jhi_user_authority` (
  `user_id` bigint(20) NOT NULL,
  `authority_name` varchar(50) NOT NULL,
  PRIMARY KEY (`user_id`,`authority_name`),
  KEY `fk_authority_name` (`authority_name`),
  CONSTRAINT `fk_authority_name` FOREIGN KEY (`authority_name`) REFERENCES `jhi_authority` (`name`),
  CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `jhi_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jhi_user_authority`
--

LOCK TABLES `jhi_user_authority` WRITE;
/*!40000 ALTER TABLE `jhi_user_authority` DISABLE KEYS */;
INSERT INTO `jhi_user_authority` VALUES (1,'ROLE_ADMIN'),(3,'ROLE_ADMIN'),(1,'ROLE_USER'),(3,'ROLE_USER'),(4,'ROLE_USER'),(19,'ROLE_USER'),(20,'ROLE_USER');
/*!40000 ALTER TABLE `jhi_user_authority` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `leave_requeste`
--

DROP TABLE IF EXISTS `leave_requeste`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leave_requeste` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `purpose` varchar(255) DEFAULT NULL,
  `create_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `expered_at` date DEFAULT NULL,
  `valided` bit(1) DEFAULT NULL,
  `employee_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_leave_requeste_employee_id` (`employee_id`),
  CONSTRAINT `fk_leave_requeste_employee_id` FOREIGN KEY (`employee_id`) REFERENCES `jhi_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leave_requeste`
--

LOCK TABLES `leave_requeste` WRITE;
/*!40000 ALTER TABLE `leave_requeste` DISABLE KEYS */;
INSERT INTO `leave_requeste` VALUES (3,'Mariageeevvv','2019-04-25',NULL,'2019-04-27','',19),(6,'repos','2019-04-27',NULL,'2019-04-28','',19),(7,'Mariageeeeeerrr','2019-05-03',NULL,'2019-05-12','',19),(9,'testttttt','2019-04-11',NULL,'2019-04-20','\0',19);
/*!40000 ALTER TABLE `leave_requeste` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-04-26 11:55:24
