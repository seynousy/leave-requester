/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { LeaverequesterTestModule } from '../../../test.module';
import { LeaveRequesteComponent } from 'app/entities/leave-requeste/leave-requeste.component';
import { LeaveRequesteService } from 'app/entities/leave-requeste/leave-requeste.service';
import { LeaveRequeste } from 'app/shared/model/leave-requeste.model';

describe('Component Tests', () => {
    describe('LeaveRequeste Management Component', () => {
        let comp: LeaveRequesteComponent;
        let fixture: ComponentFixture<LeaveRequesteComponent>;
        let service: LeaveRequesteService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [LeaverequesterTestModule],
                declarations: [LeaveRequesteComponent],
                providers: []
            })
                .overrideTemplate(LeaveRequesteComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(LeaveRequesteComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(LeaveRequesteService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new LeaveRequeste(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.leaveRequestes[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
