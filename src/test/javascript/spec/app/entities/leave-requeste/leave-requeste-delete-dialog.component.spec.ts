/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { LeaverequesterTestModule } from '../../../test.module';
import { LeaveRequesteDeleteDialogComponent } from 'app/entities/leave-requeste/leave-requeste-delete-dialog.component';
import { LeaveRequesteService } from 'app/entities/leave-requeste/leave-requeste.service';

describe('Component Tests', () => {
    describe('LeaveRequeste Management Delete Component', () => {
        let comp: LeaveRequesteDeleteDialogComponent;
        let fixture: ComponentFixture<LeaveRequesteDeleteDialogComponent>;
        let service: LeaveRequesteService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [LeaverequesterTestModule],
                declarations: [LeaveRequesteDeleteDialogComponent]
            })
                .overrideTemplate(LeaveRequesteDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(LeaveRequesteDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(LeaveRequesteService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
