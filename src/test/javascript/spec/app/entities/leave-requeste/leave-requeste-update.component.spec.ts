/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { LeaverequesterTestModule } from '../../../test.module';
import { LeaveRequesteUpdateComponent } from 'app/entities/leave-requeste/leave-requeste-update.component';
import { LeaveRequesteService } from 'app/entities/leave-requeste/leave-requeste.service';
import { LeaveRequeste } from 'app/shared/model/leave-requeste.model';

describe('Component Tests', () => {
    describe('LeaveRequeste Management Update Component', () => {
        let comp: LeaveRequesteUpdateComponent;
        let fixture: ComponentFixture<LeaveRequesteUpdateComponent>;
        let service: LeaveRequesteService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [LeaverequesterTestModule],
                declarations: [LeaveRequesteUpdateComponent]
            })
                .overrideTemplate(LeaveRequesteUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(LeaveRequesteUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(LeaveRequesteService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new LeaveRequeste(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.leaveRequeste = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new LeaveRequeste();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.leaveRequeste = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
