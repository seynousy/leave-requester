/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { LeaverequesterTestModule } from '../../../test.module';
import { LeaveRequesteDetailComponent } from 'app/entities/leave-requeste/leave-requeste-detail.component';
import { LeaveRequeste } from 'app/shared/model/leave-requeste.model';

describe('Component Tests', () => {
    describe('LeaveRequeste Management Detail Component', () => {
        let comp: LeaveRequesteDetailComponent;
        let fixture: ComponentFixture<LeaveRequesteDetailComponent>;
        const route = ({ data: of({ leaveRequeste: new LeaveRequeste(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [LeaverequesterTestModule],
                declarations: [LeaveRequesteDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(LeaveRequesteDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(LeaveRequesteDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.leaveRequeste).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
