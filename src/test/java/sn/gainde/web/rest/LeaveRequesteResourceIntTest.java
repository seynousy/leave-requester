package sn.gainde.web.rest;

import sn.gainde.LeaverequesterApp;

import sn.gainde.domain.LeaveRequeste;
import sn.gainde.repository.LeaveRequesteRepository;
import sn.gainde.service.LeaveRequesteService;
import sn.gainde.service.MailService;
import sn.gainde.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;


import static sn.gainde.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the LeaveRequesteResource REST controller.
 *
 * @see LeaveRequesteResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = LeaverequesterApp.class)
public class LeaveRequesteResourceIntTest {

    private static final String DEFAULT_PURPOSE = "AAAAAAAAAA";
    private static final String UPDATED_PURPOSE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATE_AT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATE_AT = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_UPDATED_AT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_UPDATED_AT = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_EXPERED_AT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_EXPERED_AT = LocalDate.now(ZoneId.systemDefault());

    private static final Boolean DEFAULT_VALIDED = false;
    private static final Boolean UPDATED_VALIDED = true;

    @Autowired
    private LeaveRequesteRepository leaveRequesteRepository;

    @Autowired
    private LeaveRequesteService leaveRequesteService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    @Autowired
    MailService mailService;

    private MockMvc restLeaveRequesteMockMvc;

    private LeaveRequeste leaveRequeste;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final LeaveRequesteResource leaveRequesteResource = new LeaveRequesteResource(leaveRequesteService,mailService);
        this.restLeaveRequesteMockMvc = MockMvcBuilders.standaloneSetup(leaveRequesteResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LeaveRequeste createEntity(EntityManager em) {
        LeaveRequeste leaveRequeste = new LeaveRequeste()
            .purpose(DEFAULT_PURPOSE)
            .createAt(DEFAULT_CREATE_AT)
            .updatedAt(DEFAULT_UPDATED_AT)
            .experedAt(DEFAULT_EXPERED_AT)
            .valided(DEFAULT_VALIDED);
        return leaveRequeste;
    }

    @Before
    public void initTest() {
        leaveRequeste = createEntity(em);
    }

    @Test
    @Transactional
    public void createLeaveRequeste() throws Exception {
        int databaseSizeBeforeCreate = leaveRequesteRepository.findAll().size();

        // Create the LeaveRequeste
        restLeaveRequesteMockMvc.perform(post("/api/leave-requestes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(leaveRequeste)))
            .andExpect(status().isCreated());

        // Validate the LeaveRequeste in the database
        List<LeaveRequeste> leaveRequesteList = leaveRequesteRepository.findAll();
        assertThat(leaveRequesteList).hasSize(databaseSizeBeforeCreate + 1);
        LeaveRequeste testLeaveRequeste = leaveRequesteList.get(leaveRequesteList.size() - 1);
        assertThat(testLeaveRequeste.getPurpose()).isEqualTo(DEFAULT_PURPOSE);
        assertThat(testLeaveRequeste.getCreateAt()).isEqualTo(DEFAULT_CREATE_AT);
        assertThat(testLeaveRequeste.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testLeaveRequeste.getExperedAt()).isEqualTo(DEFAULT_EXPERED_AT);
        assertThat(testLeaveRequeste.isValided()).isEqualTo(DEFAULT_VALIDED);
    }

    @Test
    @Transactional
    public void createLeaveRequesteWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = leaveRequesteRepository.findAll().size();

        // Create the LeaveRequeste with an existing ID
        leaveRequeste.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restLeaveRequesteMockMvc.perform(post("/api/leave-requestes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(leaveRequeste)))
            .andExpect(status().isBadRequest());

        // Validate the LeaveRequeste in the database
        List<LeaveRequeste> leaveRequesteList = leaveRequesteRepository.findAll();
        assertThat(leaveRequesteList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllLeaveRequestes() throws Exception {
        // Initialize the database
        leaveRequesteRepository.saveAndFlush(leaveRequeste);

        // Get all the leaveRequesteList
        restLeaveRequesteMockMvc.perform(get("/api/leave-requestes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(leaveRequeste.getId().intValue())))
            .andExpect(jsonPath("$.[*].purpose").value(hasItem(DEFAULT_PURPOSE.toString())))
            .andExpect(jsonPath("$.[*].createAt").value(hasItem(DEFAULT_CREATE_AT.toString())))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(DEFAULT_UPDATED_AT.toString())))
            .andExpect(jsonPath("$.[*].experedAt").value(hasItem(DEFAULT_EXPERED_AT.toString())))
            .andExpect(jsonPath("$.[*].valided").value(hasItem(DEFAULT_VALIDED.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getLeaveRequeste() throws Exception {
        // Initialize the database
        leaveRequesteRepository.saveAndFlush(leaveRequeste);

        // Get the leaveRequeste
        restLeaveRequesteMockMvc.perform(get("/api/leave-requestes/{id}", leaveRequeste.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(leaveRequeste.getId().intValue()))
            .andExpect(jsonPath("$.purpose").value(DEFAULT_PURPOSE.toString()))
            .andExpect(jsonPath("$.createAt").value(DEFAULT_CREATE_AT.toString()))
            .andExpect(jsonPath("$.updatedAt").value(DEFAULT_UPDATED_AT.toString()))
            .andExpect(jsonPath("$.experedAt").value(DEFAULT_EXPERED_AT.toString()))
            .andExpect(jsonPath("$.valided").value(DEFAULT_VALIDED.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingLeaveRequeste() throws Exception {
        // Get the leaveRequeste
        restLeaveRequesteMockMvc.perform(get("/api/leave-requestes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLeaveRequeste() throws Exception {
        // Initialize the database
        leaveRequesteService.save(leaveRequeste);

        int databaseSizeBeforeUpdate = leaveRequesteRepository.findAll().size();

        // Update the leaveRequeste
        LeaveRequeste updatedLeaveRequeste = leaveRequesteRepository.findById(leaveRequeste.getId()).get();
        // Disconnect from session so that the updates on updatedLeaveRequeste are not directly saved in db
        em.detach(updatedLeaveRequeste);
        updatedLeaveRequeste
            .purpose(UPDATED_PURPOSE)
            .createAt(UPDATED_CREATE_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .experedAt(UPDATED_EXPERED_AT)
            .valided(UPDATED_VALIDED);

        restLeaveRequesteMockMvc.perform(put("/api/leave-requestes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedLeaveRequeste)))
            .andExpect(status().isOk());

        // Validate the LeaveRequeste in the database
        List<LeaveRequeste> leaveRequesteList = leaveRequesteRepository.findAll();
        assertThat(leaveRequesteList).hasSize(databaseSizeBeforeUpdate);
        LeaveRequeste testLeaveRequeste = leaveRequesteList.get(leaveRequesteList.size() - 1);
        assertThat(testLeaveRequeste.getPurpose()).isEqualTo(UPDATED_PURPOSE);
        assertThat(testLeaveRequeste.getCreateAt()).isEqualTo(UPDATED_CREATE_AT);
        assertThat(testLeaveRequeste.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testLeaveRequeste.getExperedAt()).isEqualTo(UPDATED_EXPERED_AT);
        assertThat(testLeaveRequeste.isValided()).isEqualTo(UPDATED_VALIDED);
    }

    @Test
    @Transactional
    public void updateNonExistingLeaveRequeste() throws Exception {
        int databaseSizeBeforeUpdate = leaveRequesteRepository.findAll().size();

        // Create the LeaveRequeste

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLeaveRequesteMockMvc.perform(put("/api/leave-requestes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(leaveRequeste)))
            .andExpect(status().isBadRequest());

        // Validate the LeaveRequeste in the database
        List<LeaveRequeste> leaveRequesteList = leaveRequesteRepository.findAll();
        assertThat(leaveRequesteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteLeaveRequeste() throws Exception {
        // Initialize the database
        leaveRequesteService.save(leaveRequeste);

        int databaseSizeBeforeDelete = leaveRequesteRepository.findAll().size();

        // Delete the leaveRequeste
        restLeaveRequesteMockMvc.perform(delete("/api/leave-requestes/{id}", leaveRequeste.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<LeaveRequeste> leaveRequesteList = leaveRequesteRepository.findAll();
        assertThat(leaveRequesteList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(LeaveRequeste.class);
        LeaveRequeste leaveRequeste1 = new LeaveRequeste();
        leaveRequeste1.setId(1L);
        LeaveRequeste leaveRequeste2 = new LeaveRequeste();
        leaveRequeste2.setId(leaveRequeste1.getId());
        assertThat(leaveRequeste1).isEqualTo(leaveRequeste2);
        leaveRequeste2.setId(2L);
        assertThat(leaveRequeste1).isNotEqualTo(leaveRequeste2);
        leaveRequeste1.setId(null);
        assertThat(leaveRequeste1).isNotEqualTo(leaveRequeste2);
    }
}
