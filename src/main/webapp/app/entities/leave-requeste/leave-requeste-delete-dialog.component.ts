import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ILeaveRequeste } from 'app/shared/model/leave-requeste.model';
import { LeaveRequesteService } from './leave-requeste.service';

@Component({
    selector: 'jhi-leave-requeste-delete-dialog',
    templateUrl: './leave-requeste-delete-dialog.component.html'
})
export class LeaveRequesteDeleteDialogComponent {
    leaveRequeste: ILeaveRequeste;

    constructor(
        protected leaveRequesteService: LeaveRequesteService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.leaveRequesteService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'leaveRequesteListModification',
                content: 'Deleted an leaveRequeste'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-leave-requeste-delete-popup',
    template: ''
})
export class LeaveRequesteDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ leaveRequeste }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(LeaveRequesteDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.leaveRequeste = leaveRequeste;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/leave-requeste', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/leave-requeste', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
