import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { JhiAlertService } from 'ng-jhipster';
import { ILeaveRequeste } from 'app/shared/model/leave-requeste.model';
import { LeaveRequesteService } from './leave-requeste.service';
import {AccountService, IUser, UserService} from 'app/core';

@Component({
    selector: 'jhi-leave-requeste-update',
    templateUrl: './leave-requeste-update.component.html'
})
export class LeaveRequesteUpdateComponent implements OnInit {
    leaveRequeste: ILeaveRequeste;
    isSaving: boolean;

    users: IUser[];
    account: any;
    createAtDp: any;
    updatedAtDp: any;
    experedAtDp: any;

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected leaveRequesteService: LeaveRequesteService,
        protected userService: UserService,
        protected activatedRoute: ActivatedRoute,
        private accountService: AccountService
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.accountService.identity().then(account => {
            this.account = account;
            this.leaveRequeste.employee = account;
        });
        this.activatedRoute.data.subscribe(({ leaveRequeste }) => {
            this.leaveRequeste = leaveRequeste;
        });
        this.userService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IUser[]>) => mayBeOk.ok),
                map((response: HttpResponse<IUser[]>) => response.body)
            )
            .subscribe((res: IUser[]) => (this.users = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.leaveRequeste.id !== undefined) {
            this.subscribeToSaveResponse(this.leaveRequesteService.update(this.leaveRequeste));
        } else {
            this.subscribeToSaveResponse(this.leaveRequesteService.create(this.leaveRequeste));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<ILeaveRequeste>>) {
        result.subscribe((res: HttpResponse<ILeaveRequeste>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackUserById(index: number, item: IUser) {
        return item.id;
    }
}
