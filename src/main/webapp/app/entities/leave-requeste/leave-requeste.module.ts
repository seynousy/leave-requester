import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { LeaverequesterSharedModule } from 'app/shared';
import {
    LeaveRequesteComponent,
    LeaveRequesteDetailComponent,
    LeaveRequesteUpdateComponent,
    LeaveRequesteDeletePopupComponent,
    LeaveRequesteDeleteDialogComponent,
    leaveRequesteRoute,
    leaveRequestePopupRoute
} from './';
import {CallbackPipe} from "app/entities/leave-requeste/callback.pipe";

const ENTITY_STATES = [...leaveRequesteRoute, ...leaveRequestePopupRoute];

@NgModule({
    imports: [LeaverequesterSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        LeaveRequesteComponent,
        LeaveRequesteDetailComponent,
        LeaveRequesteUpdateComponent,
        LeaveRequesteDeleteDialogComponent,
        LeaveRequesteDeletePopupComponent,
        CallbackPipe
    ],
    entryComponents: [
        LeaveRequesteComponent,
        LeaveRequesteUpdateComponent,
        LeaveRequesteDeleteDialogComponent,
        LeaveRequesteDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class LeaverequesterLeaveRequesteModule {}
