import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import {Observable, Subscription} from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ILeaveRequeste } from 'app/shared/model/leave-requeste.model';
import {AccountService, IUser} from 'app/core';
import { LeaveRequesteService } from './leave-requeste.service';

@Component({
    selector: 'jhi-leave-requeste',
    templateUrl: './leave-requeste.component.html'
})
export class LeaveRequesteComponent implements OnInit, OnDestroy {
    leaveRequestes: ILeaveRequeste[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected leaveRequesteService: LeaveRequesteService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.leaveRequesteService
            .query()
            .pipe(
                filter((res: HttpResponse<ILeaveRequeste[]>) => res.ok),
                map((res: HttpResponse<ILeaveRequeste[]>) => res.body)
            )
            .subscribe(
                (res: ILeaveRequeste[]) => {
                    this.leaveRequestes = res;
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInLeaveRequestes();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ILeaveRequeste) {
        return item.id;
    }

    registerChangeInLeaveRequestes() {
        this.eventSubscriber = this.eventManager.subscribe('leaveRequesteListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<ILeaveRequeste>>) {
        result.subscribe((res: HttpResponse<ILeaveRequeste>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }
    protected onSaveSuccess() {

    }

    protected onSaveError() {

    }
    valider(leaveRequeste){
        leaveRequeste.valided = true;
        this.subscribeToSaveResponse(this.leaveRequesteService.update(leaveRequeste));
    }

    isAuthenticated() {
        return this.accountService.isAuthenticated();
    }

}
