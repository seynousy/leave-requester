import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ILeaveRequeste } from 'app/shared/model/leave-requeste.model';

@Component({
    selector: 'jhi-leave-requeste-detail',
    templateUrl: './leave-requeste-detail.component.html'
})
export class LeaveRequesteDetailComponent implements OnInit {
    leaveRequeste: ILeaveRequeste;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ leaveRequeste }) => {
            this.leaveRequeste = leaveRequeste;
        });
    }

    previousState() {
        window.history.back();
    }
}
