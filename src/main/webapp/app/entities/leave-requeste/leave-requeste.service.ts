import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ILeaveRequeste } from 'app/shared/model/leave-requeste.model';

type EntityResponseType = HttpResponse<ILeaveRequeste>;
type EntityArrayResponseType = HttpResponse<ILeaveRequeste[]>;

@Injectable({ providedIn: 'root' })
export class LeaveRequesteService {
    public resourceUrl = SERVER_API_URL + 'api/leave-requestes';

    constructor(protected http: HttpClient) {}

    create(leaveRequeste: ILeaveRequeste): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(leaveRequeste);
        return this.http
            .post<ILeaveRequeste>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(leaveRequeste: ILeaveRequeste): Observable<EntityResponseType> {
        console.log("cboooooooooon");
        const copy = this.convertDateFromClient(leaveRequeste);
        console.log("cboooooooooon");
        return this.http
            .put<ILeaveRequeste>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<ILeaveRequeste>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<ILeaveRequeste[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    protected convertDateFromClient(leaveRequeste: ILeaveRequeste): ILeaveRequeste {
        const copy: ILeaveRequeste = Object.assign({}, leaveRequeste, {
            createAt:
                leaveRequeste.createAt != null && leaveRequeste.createAt.isValid() ? leaveRequeste.createAt.format(DATE_FORMAT) : null,
            updatedAt:
                leaveRequeste.updatedAt != null && leaveRequeste.updatedAt.isValid() ? leaveRequeste.updatedAt.format(DATE_FORMAT) : null,
            experedAt:
                leaveRequeste.experedAt != null && leaveRequeste.experedAt.isValid() ? leaveRequeste.experedAt.format(DATE_FORMAT) : null
        });
        return copy;
    }

    protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
        if (res.body) {
            res.body.createAt = res.body.createAt != null ? moment(res.body.createAt) : null;
            res.body.updatedAt = res.body.updatedAt != null ? moment(res.body.updatedAt) : null;
            res.body.experedAt = res.body.experedAt != null ? moment(res.body.experedAt) : null;
        }
        return res;
    }

    protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        if (res.body) {
            res.body.forEach((leaveRequeste: ILeaveRequeste) => {
                leaveRequeste.createAt = leaveRequeste.createAt != null ? moment(leaveRequeste.createAt) : null;
                leaveRequeste.updatedAt = leaveRequeste.updatedAt != null ? moment(leaveRequeste.updatedAt) : null;
                leaveRequeste.experedAt = leaveRequeste.experedAt != null ? moment(leaveRequeste.experedAt) : null;
            });
        }
        return res;
    }
}
