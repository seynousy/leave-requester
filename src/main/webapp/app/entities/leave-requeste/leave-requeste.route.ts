import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { LeaveRequeste } from 'app/shared/model/leave-requeste.model';
import { LeaveRequesteService } from './leave-requeste.service';
import { LeaveRequesteComponent } from './leave-requeste.component';
import { LeaveRequesteDetailComponent } from './leave-requeste-detail.component';
import { LeaveRequesteUpdateComponent } from './leave-requeste-update.component';
import { LeaveRequesteDeletePopupComponent } from './leave-requeste-delete-dialog.component';
import { ILeaveRequeste } from 'app/shared/model/leave-requeste.model';

@Injectable({ providedIn: 'root' })
export class LeaveRequesteResolve implements Resolve<ILeaveRequeste> {
    constructor(private service: LeaveRequesteService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ILeaveRequeste> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<LeaveRequeste>) => response.ok),
                map((leaveRequeste: HttpResponse<LeaveRequeste>) => leaveRequeste.body)
            );
        }
        return of(new LeaveRequeste());
    }
}

export const leaveRequesteRoute: Routes = [
    {
        path: '',
        component: LeaveRequesteComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'LeaveRequestes'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: LeaveRequesteDetailComponent,
        resolve: {
            leaveRequeste: LeaveRequesteResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'LeaveRequestes'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: LeaveRequesteUpdateComponent,
        resolve: {
            leaveRequeste: LeaveRequesteResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'LeaveRequestes'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: LeaveRequesteUpdateComponent,
        resolve: {
            leaveRequeste: LeaveRequesteResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'LeaveRequestes'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const leaveRequestePopupRoute: Routes = [
    {
        path: ':id/delete',
        component: LeaveRequesteDeletePopupComponent,
        resolve: {
            leaveRequeste: LeaveRequesteResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'LeaveRequestes'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
