import { Moment } from 'moment';
import { IUser } from 'app/core/user/user.model';

export interface ILeaveRequeste {
    id?: number;
    purpose?: string;
    createAt?: Moment;
    updatedAt?: Moment;
    experedAt?: Moment;
    valided?: boolean;
    employee?: IUser;
}

export class LeaveRequeste implements ILeaveRequeste {
    constructor(
        public id?: number,
        public purpose?: string,
        public createAt?: Moment,
        public updatedAt?: Moment,
        public experedAt?: Moment,
        public valided?: boolean,
        public employee?: IUser
    ) {
        this.valided = this.valided || false;
    }
}
