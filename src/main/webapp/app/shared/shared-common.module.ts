import { NgModule } from '@angular/core';

import { LeaverequesterSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
    imports: [LeaverequesterSharedLibsModule],
    declarations: [JhiAlertComponent, JhiAlertErrorComponent],
    exports: [LeaverequesterSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class LeaverequesterSharedCommonModule {}
