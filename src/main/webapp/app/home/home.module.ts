import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { LeaverequesterSharedModule } from 'app/shared';
import {HOME_ROUTE, HomeComponent} from './';
import {CallbackPipe} from "./callback.pipe";

@NgModule({
    imports: [LeaverequesterSharedModule, RouterModule.forChild([{
        path: '',
        loadChildren: '../entities/leave-requeste/leave-requeste.module#LeaverequesterLeaveRequesteModule'
    }])],
    declarations: [HomeComponent,CallbackPipe],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class LeaverequesterHomeModule {}