package sn.gainde.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A LeaveRequeste.
 */
@Entity
@Table(name = "leave_requeste")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class LeaveRequeste implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "purpose")
    private String purpose;

    @Column(name = "create_at")
    private LocalDate createAt;

    @Column(name = "updated_at")
    private LocalDate updatedAt;

    @Column(name = "expered_at")
    private LocalDate experedAt;

    @Column(name = "valided")
    private Boolean valided;

    @ManyToOne
    @JsonIgnoreProperties("leaveRequestes")
    private User employee;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPurpose() {
        return purpose;
    }

    public LeaveRequeste purpose(String purpose) {
        this.purpose = purpose;
        return this;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public LocalDate getCreateAt() {
        return createAt;
    }

    public LeaveRequeste createAt(LocalDate createAt) {
        this.createAt = createAt;
        return this;
    }

    public void setCreateAt(LocalDate createAt) {
        this.createAt = createAt;
    }

    public LocalDate getUpdatedAt() {
        return updatedAt;
    }

    public LeaveRequeste updatedAt(LocalDate updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public void setUpdatedAt(LocalDate updatedAt) {
        this.updatedAt = updatedAt;
    }

    public LocalDate getExperedAt() {
        return experedAt;
    }

    public LeaveRequeste experedAt(LocalDate experedAt) {
        this.experedAt = experedAt;
        return this;
    }

    public void setExperedAt(LocalDate experedAt) {
        this.experedAt = experedAt;
    }

    public Boolean isValided() {
        return valided;
    }

    public LeaveRequeste valided(Boolean valided) {
        this.valided = valided;
        return this;
    }

    public void setValided(Boolean valided) {
        this.valided = valided;
    }

    public User getEmployee() {
        return employee;
    }

    public LeaveRequeste employee(User user) {
        this.employee = user;
        return this;
    }

    public void setEmployee(User user) {
        this.employee = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        LeaveRequeste leaveRequeste = (LeaveRequeste) o;
        if (leaveRequeste.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), leaveRequeste.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "LeaveRequeste{" +
            "id=" + getId() +
            ", purpose='" + getPurpose() + "'" +
            ", createAt='" + getCreateAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", experedAt='" + getExperedAt() + "'" +
            ", valided='" + isValided() + "'" +
            "}";
    }
}
