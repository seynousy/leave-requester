package sn.gainde.service;

import sn.gainde.domain.LeaveRequeste;
import sn.gainde.repository.LeaveRequesteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing LeaveRequeste.
 */
@Service
@Transactional
public class LeaveRequesteService {

    private final Logger log = LoggerFactory.getLogger(LeaveRequesteService.class);

    private final LeaveRequesteRepository leaveRequesteRepository;

    public LeaveRequesteService(LeaveRequesteRepository leaveRequesteRepository) {
        this.leaveRequesteRepository = leaveRequesteRepository;
    }

    /**
     * Save a leaveRequeste.
     *
     * @param leaveRequeste the entity to save
     * @return the persisted entity
     */
    public LeaveRequeste save(LeaveRequeste leaveRequeste) {
        log.debug("Request to save LeaveRequeste : {}", leaveRequeste);
        return leaveRequesteRepository.save(leaveRequeste);
    }

    /**
     * Get all the leaveRequestes.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<LeaveRequeste> findAll() {
        log.debug("Request to get all LeaveRequestes");
        return leaveRequesteRepository.findAll();
    }


    /**
     * Get one leaveRequeste by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<LeaveRequeste> findOne(Long id) {
        log.debug("Request to get LeaveRequeste : {}", id);
        return leaveRequesteRepository.findById(id);
    }

    /**
     * Delete the leaveRequeste by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete LeaveRequeste : {}", id);
        leaveRequesteRepository.deleteById(id);
    }
}
