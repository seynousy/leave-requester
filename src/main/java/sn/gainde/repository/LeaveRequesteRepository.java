package sn.gainde.repository;

import sn.gainde.domain.LeaveRequeste;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the LeaveRequeste entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LeaveRequesteRepository extends JpaRepository<LeaveRequeste, Long> {

    @Query("select leave_requeste from LeaveRequeste leave_requeste where leave_requeste.employee.login = ?#{principal.username}")
    List<LeaveRequeste> findByEmployeeIsCurrentUser();

}
