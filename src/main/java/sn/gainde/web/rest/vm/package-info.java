/**
 * View Models used by Spring MVC REST controllers.
 */
package sn.gainde.web.rest.vm;
