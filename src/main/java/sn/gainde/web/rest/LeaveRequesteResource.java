package sn.gainde.web.rest;
import org.springframework.scheduling.annotation.Async;
import sn.gainde.domain.LeaveRequeste;
import sn.gainde.service.LeaveRequesteService;
import sn.gainde.service.MailService;
import sn.gainde.web.rest.errors.BadRequestAlertException;
import sn.gainde.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.mail.internet.MimeMessage;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing LeaveRequeste.
 */
@RestController
@RequestMapping("/api")
public class LeaveRequesteResource {

    private final Logger log = LoggerFactory.getLogger(LeaveRequesteResource.class);

    private static final String ENTITY_NAME = "leaveRequeste";

    private final LeaveRequesteService leaveRequesteService;

    MailService  mailService;

    public LeaveRequesteResource(LeaveRequesteService leaveRequesteService, MailService mailService) {
        this.leaveRequesteService = leaveRequesteService;
        this.mailService = mailService;
    }

    /**
     * POST  /leave-requestes : Create a new leaveRequeste.
     *
     * @param leaveRequeste the leaveRequeste to create
     * @return the ResponseEntity with status 201 (Created) and with body the new leaveRequeste, or with status 400 (Bad Request) if the leaveRequeste has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/leave-requestes")
    public ResponseEntity<LeaveRequeste> createLeaveRequeste(@RequestBody LeaveRequeste leaveRequeste) throws URISyntaxException {
        log.debug("REST request to save LeaveRequeste : {}", leaveRequeste);
        if (leaveRequeste.getId() != null) {
            throw new BadRequestAlertException("A new leaveRequeste cannot already have an ID", ENTITY_NAME, "idexists");
        }
        LeaveRequeste result = leaveRequesteService.save(leaveRequeste);
        return ResponseEntity.created(new URI("/api/leave-requestes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /leave-requestes : Updates an existing leaveRequeste.
     *
     * @param leaveRequeste the leaveRequeste to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated leaveRequeste,
     * or with status 400 (Bad Request) if the leaveRequeste is not valid,
     * or with status 500 (Internal Server Error) if the leaveRequeste couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/leave-requestes")
    public ResponseEntity<LeaveRequeste> updateLeaveRequeste(@RequestBody LeaveRequeste leaveRequeste) throws URISyntaxException {
        System.err.println("cbooooooonnnnnnnnn");
        log.debug("REST request to update LeaveRequeste : {}", leaveRequeste);
        if (leaveRequeste.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (leaveRequeste.isValided())
            mailService.sendvalidetedLeaveRequeste(leaveRequeste.getEmployee(),leaveRequeste);

        LeaveRequeste result = leaveRequesteService.save(leaveRequeste);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, leaveRequeste.getId().toString()))
            .body(result);
    }


    /**
     * GET  /leave-requestes : get all the leaveRequestes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of leaveRequestes in body
     */
    @GetMapping("/leave-requestes")
    public List<LeaveRequeste> getAllLeaveRequestes() {
        log.debug("REST request to get all LeaveRequestes");
        return leaveRequesteService.findAll();
    }

    /**
     * GET  /leave-requestes/:id : get the "id" leaveRequeste.
     *
     * @param id the id of the leaveRequeste to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the leaveRequeste, or with status 404 (Not Found)
     */
    @GetMapping("/leave-requestes/{id}")
    public ResponseEntity<LeaveRequeste> getLeaveRequeste(@PathVariable Long id) {
        log.debug("REST request to get LeaveRequeste : {}", id);
        Optional<LeaveRequeste> leaveRequeste = leaveRequesteService.findOne(id);
        return ResponseUtil.wrapOrNotFound(leaveRequeste);
    }

    /**
     * DELETE  /leave-requestes/:id : delete the "id" leaveRequeste.
     *
     * @param id the id of the leaveRequeste to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/leave-requestes/{id}")
    public ResponseEntity<Void> deleteLeaveRequeste(@PathVariable Long id) {
        log.debug("REST request to delete LeaveRequeste : {}", id);
        Optional<LeaveRequeste> oLeaveRequeste = leaveRequesteService.findOne(id);
        if (oLeaveRequeste.isPresent())
            mailService.sendRefusedLeaveRequeste(oLeaveRequeste.get().getEmployee(), oLeaveRequeste.get());
        leaveRequesteService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
